'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider',
      function ($stateProvider,   $urlRouterProvider) {
          
          $urlRouterProvider
              .otherwise('/app/contacts');
          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/app',
                  templateUrl: 'tpl/app.html'
              })
              .state('app.contacts', {
                  url: '/contacts',
                  templateUrl: 'tpl/contacts.html',
                  resolve: {
                    deps: ['uiLoad',
                      function( uiLoad ){
                        return uiLoad.load( [
                          'js/services/bookmark-service.js',
                          'js/app/contacts/contacts-service.js',
                          'js/app/contacts/contacts.js',
                          'vendor/libs/moment.min.js'] );
                      }]
                  }
              })
              .state('app.workflow', {
                  url: '/workflow',
                  templateUrl: 'tpl/workflow.html',
                  resolve: {
                     deps: ['uiLoad',
                     function( uiLoad ){
                         return uiLoad.load( [
                           'js/app/workflow/workflow.js',
                           'js/app/workflow/workflow-service.js',
                           'vendor/libs/moment.min.js'] );
                     }]
                 }
              })
              .state('app.workflow.pc', {
                  url: '/pc',
                  templateUrl: 'tpl/workflow_pc.html'
              })
              .state('app.workflow.bp', {
                url: '/bp',
                templateUrl: 'tpl/workflow_bp.html'
              })
      }
    ]
  );