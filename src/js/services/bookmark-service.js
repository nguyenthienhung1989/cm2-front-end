
app.service('BookmarkService', function($http) {

  this.getBookmarks = function() {

    var bookmarks = [];

    bookmarks[0] = {
      name: "Lead with Rank Higher than 2",
      filters: [
        {
          property: "rank",
          operator: "greater than",
          values: [2]
        },
        {
          property: "user_type",
          operator: "equals",
          values: ["PC Lead", "BP Lead"]
        }
      ]
    };

  }

});